#!/usr/bin/env python3

from dataclasses import dataclass
from typing import Tuple, Type


@dataclass
class LinkedListNode:
    value: int
    next_node: Type["LinkedListNode"]


def print_linked_list_w_loop(
    head: LinkedListNode, loop_head: LinkedListNode
) -> None:
    print_curr = head
    found_loop_head = False
    while True:
        if print_curr == loop_head:
            if not found_loop_head:
                found_loop_head = True
                print("loop_head", end=" ")
            else:
                print("looped back to loop_head")
                break

        print(print_curr.value)
        print_curr = print_curr.next_node


def create_linked_list_w_loop(
    m: int, n: int
) -> Tuple[LinkedListNode, LinkedListNode]:
    assert n >= n
    assert m > n

    # 10
    # 5

    # 0, 1, 2, 3, 4, 5, 6, 7, 8, 9

    head = LinkedListNode(0, None)
    curr = head
    loop_head = None

    for i in range(m - 1):
        curr.next_node = LinkedListNode(i + 1, None)
        curr = curr.next_node
        if n != 0 and i == m - n - 1:
            loop_head = curr

    curr.next_node = loop_head

    return head, loop_head


def detect_loop(head: LinkedListNode) -> bool:
    has_loop = False
    hare_node = head
    tortoise_node = head
    while (
        tortoise_node.next_node is not None
        and hare_node.next_node is not None
        and hare_node.next_node.next_node is not None
    ):
        tortoise_node = tortoise_node.next_node
        hare_node = hare_node.next_node.next_node
        if tortoise_node == hare_node:
            has_loop = True
            break
    return has_loop


if __name__ == "__main__":
    head, loop_head = create_linked_list_w_loop(10, 5)
    print_linked_list_w_loop(head, loop_head)
    print(detect_loop(head))
