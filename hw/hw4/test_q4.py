#!/usr/bin/env python3

from dataclasses import dataclass
from math import isnan
from typing import Dict, Set, Tuple

INVALID_WEIGHT = float("nan")


@dataclass(frozen=True)
class Edge:
    from_vertex: int
    to_vertex: int
    weight: int


def have_edge(
    edges: Set[Edge],
    memo: Dict[Tuple[int, int], Tuple[int, int]],
    curr_k: int,
    to_vertex: int,
) -> bool:
    does_have_edge = False
    for edge in edges:
        curr_prev_vertex = edge.from_vertex
        if edge.to_vertex == to_vertex and not isnan(
            memo[(curr_k - 1, curr_prev_vertex)][0]
        ):
            does_have_edge = True
            break

    return does_have_edge


def best_prev_edge(
    edges: Set[Edge],
    memo: Dict[Tuple[int, int], Tuple[int, int]],
    curr_k: int,
    to_vertex: int,
) -> Edge:
    max_weight = -1
    best_edge = None

    for edge in edges:
        prev_vertex = edge.from_vertex
        max_weight_prev_vertex = memo[(curr_k - 1, prev_vertex)][0]

        if (
            edge.from_vertex == prev_vertex
            and edge.to_vertex == to_vertex
            and not isnan(max_weight_prev_vertex)
            and max_weight_prev_vertex + edge.weight > max_weight
        ):
            max_weight = max_weight_prev_vertex + edge.weight
            best_edge = edge

    return best_edge


def max_weight_from_prev_vertex(
    edges: Set[Edge],
    memo: Dict[Tuple[int, int], Tuple[int, int]],
    curr_k: int,
    to_vertex: int,
) -> Tuple[int, int]:
    e_v_j = best_prev_edge(edges, memo, curr_k, to_vertex)
    v = e_v_j.from_vertex
    max_weight = e_v_j.weight + memo[(curr_k - 1, v)][0]

    return (max_weight, v)


def maximise_weight(vertices: Set[int], edges: Set[Edge], k: int) -> int:
    memo = {}

    for curr_k in range(0, k + 1):
        for to_vertex in vertices:
            if curr_k == 0:
                memo[(curr_k, to_vertex)] = (0, -1)
            else:
                memo_entry = (INVALID_WEIGHT, -1)
                if have_edge(edges, memo, curr_k, to_vertex):
                    memo_entry = max_weight_from_prev_vertex(
                        edges, memo, curr_k, to_vertex
                    )

                memo[(curr_k, to_vertex)] = memo_entry

    max_weight = -1
    best_end_vertex = -1
    for vertex in vertices:
        if memo[(k, vertex)][0] > max_weight:
            max_weight = memo[(k, vertex)][0]
            best_end_vertex = vertex

    path = [best_end_vertex]
    curr_vertex = best_end_vertex
    curr_k = k
    while memo[(curr_k, curr_vertex)][1] != -1:
        path.insert(0, memo[(curr_k, curr_vertex)][1])
        curr_vertex = memo[(curr_k, curr_vertex)][1]
        curr_k -= 1

    print(f"max_weight: {max_weight}")
    print(f"path:       {path}")

    return path


def q4():
    vertices = set([0, 1, 2, 3])
    edges = set(
        [
            Edge(0, 1, 10),
            Edge(0, 1, 1),
            Edge(0, 2, 3),
            Edge(0, 3, 2),
            Edge(1, 3, 7),
            Edge(2, 3, 6),
        ]
    )
    k = 2

    # vertices = set([1, 2, 3, 4, 5, 6, 7, 8, 9])
    # edges = set(
    #     [
    #         Edge(1, 2, 0),
    #         Edge(2, 3, 0),
    #         Edge(3, 4, 0),
    #         Edge(4, 5, 100),
    #         Edge(1, 6, 1),
    #         Edge(6, 7, 1),
    #         Edge(7, 8, 1),
    #         Edge(8, 9, 1),
    #     ]
    # )
    # k = 4

    # vertices = set([1, 2, 3])
    # edges = set([Edge(1, 2, 3), Edge(2, 1, 1), Edge(1, 3, 5), Edge(3, 2, 1),])
    # k = 2

    maximise_weight(vertices, edges, k)


if __name__ == "__main__":
    q4()
