#!/usr/bin/env python3


def is_valid_expression(expression: str) -> bool:
    paren_count = 0
    is_valid = True

    for char in expression:
        if char == "(":
            paren_count += 1
        elif char == ")":
            paren_count -= 1

        if paren_count < 0:
            is_valid = False
            break

    if paren_count != 0:
        is_valid = False

    return is_valid


if __name__ == "__main__":
    print(is_valid_expression("((xy)z)w"))
